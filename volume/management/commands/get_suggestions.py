import csv
import datetime

import pytz
from django.core.management.base import BaseCommand
from django.db.models import Q
from questrade_api import Questrade

from volume.models import Volume
import yfinance as yf


class Command(BaseCommand):

    def handle(self, *args, **options):
        q = Questrade()
        tickers_to_ignore = []
        suggestions = []
        start = datetime.datetime(2021, 3, 9, 9, 30, 00, tzinfo=pytz.timezone('America/New_York')).isoformat()
        end = datetime.datetime(2021, 3, 9, 9, 31, 00, tzinfo=pytz.timezone('America/New_York')).isoformat()
        with open('tsx-venture-composite-index-02-27-2021.csv', mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                tickers_to_ignore.append(row.get("Symbol")[:-1])

        tickers_to_check = Volume.objects.filter(
            last_close_price__lt=0.3
        ).exclude(
            Q(avg_volume=0) | Q(avg_volume__isnull=True) | Q(ticker__in=tickers_to_ignore)
        )

        remaining = tickers_to_check.count()

        for ticker in tickers_to_check:
            print("_______________________________________________________________")
            print("Ticker: ", ticker.ticker)
            symbol_id = q.symbols(names=ticker.ticker + "N")['symbols'][0]['symbolId']

            candle = q.markets_candles(symbol_id, startTime=start, endTime=end, interval="OneMinute").get("candles")
            print("Candle: ", candle)

            if candle and len(candle) > 0:
                current_volume = candle[0]["volume"]
                current_close = candle[0]["close"]

                print("Average Volume: ", ticker.avg_volume)
                print("Last Close Price: ", ticker.last_close_price)
                print("Current Volume: ", current_volume)
                print("Current Close: ", current_close)

                if current_volume > 200000 and current_volume > ((0.3 * ticker.avg_volume) + ticker.avg_volume):
                    if current_close > ticker.last_close_price:
                        suggestions.append(ticker.ticker)

            remaining -= 1
            print("Remaining: ", remaining)

        print("Suggestions: ", suggestions)
