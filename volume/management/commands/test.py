import csv
import datetime
from django.core.management.base import BaseCommand
from django.db.models import Q
from volume.models import Volume
import yfinance as yf
from questrade_api import Questrade
import pytz


class Command(BaseCommand):

    def handle(self, *args, **options):
        q = Questrade()
        start = datetime.datetime(2021, 2, 24, 9, 30, 00, tzinfo=pytz.timezone('America/New_York')).isoformat()
        end = datetime.datetime(2021, 2, 24, 9, 31, 00, tzinfo=pytz.timezone('America/New_York')).isoformat()

        tickers_to_ignore = []
        suggestions = []
        with open('tsx-venture-composite-index-02-27-2021.csv', mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                tickers_to_ignore.append(row.get("Symbol")[:-1])

        volumes = Volume.objects.filter(
            last_close_price__lt=0.3
        ).exclude(
            Q(avg_volume=0) | Q(avg_volume__isnull=True) | Q(ticker__in=tickers_to_ignore)
        )
        total_count = volumes.count()

        for v in volumes:
            # FOR AVERAGE VOLUME
            ticker = yf.Ticker(v.ticker)
            print("_________________________________________")
            total_count -= 1
            print("Remaining: ", total_count)
            print("Ticker: ", ticker)
            h = ticker.history(interval='1d', start="2021-02-17", end="2021-02-23")

            if h.empty:
                v.avg_volume = None
                v.last_close_price = None
                v.save()
                continue

            volume = 0
            index = 0
            close = 0
            for i, row in h.iterrows():
                print("row: ", row['Volume'])
                volume += row['Volume']
                close = row['Close']
                index += 1

            print("Close: ", close)
            average_volume = volume / (index + 1)

            # FOR CURRENT VOLUME

            symbol_id = q.symbols(names=v.ticker + "N")['symbols'][0]['symbolId']

            candle = q.markets_candles(symbol_id, startTime=start, endTime=end, interval="OneMinute").get("candles")

            if candle and len(candle) > 0:
                current_volume = candle[0]["volume"]
                current_close = candle[0]["close"]

                if current_volume > 5000000 and current_volume > ((0.5 * average_volume) + average_volume):
                    if current_close > close:
                        suggestions.append(ticker.ticker)

        print("Suggestions: ", suggestions)
