from django.core.management.base import BaseCommand
from volume.models import Volume
import yfinance as yf


class Command(BaseCommand):

    def handle(self, *args, **options):
        volumes = Volume.objects.all()
        total_count = volumes.count()

        for v in volumes:
            ticker = yf.Ticker(v.ticker)
            print("_________________________________________")
            print("Ticker: ", ticker)
            h = ticker.history(period='1wk', interval='1d')

            if h.empty:
                v.avg_volume = None
                v.last_close_price = None
                v.save()
                continue

            volume = 0
            index = 0
            close = 0
            for i, row in h.iterrows():
                print("row: ", row['Volume'])
                volume += row['Volume']
                close = row['Close']
                index += 1

            print("Close: ", close)

            try:
                print("volume: ",  volume/(index + 1))
                v.avg_volume = volume/(index + 1)
                v.last_close_price = close
                v.save()
                total_count -= 1
            except (ZeroDivisionError, ValueError):
                total_count -= 1

            print("Total Remaining: ", total_count)
