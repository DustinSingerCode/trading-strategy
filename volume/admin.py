from django.contrib import admin
from volume.models import Volume


class VolumeAdmin(admin.ModelAdmin):
    list_display = ("ticker", 'avg_volume', 'last_close_price')


admin.site.register(Volume, VolumeAdmin)
