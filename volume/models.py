from django.db import models


# Create your models here.
class Volume(models.Model):
    ticker = models.CharField(max_length=8)
    avg_volume = models.IntegerField(null=True, blank=True)
    check = models.BooleanField(default=True)
    last_close_price = models.FloatField(null=True, blank=True)
